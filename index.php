<?php

declare(strict_types=1);

use App\User\Infrastructure\Repository\InMemoryRepository;
use App\User\Infrastructure\Repository\InMemoryUserEventsRepository;
use App\User\UseCase\UserService;

require_once __DIR__ . '/bootstrap.php';

$inMemoryRepository = new InMemoryRepository();
$inMemoryUserEventsRepository = new InMemoryUserEventsRepository();
$userService = new UserService($inMemoryRepository, $inMemoryUserEventsRepository);


$user = $userService->createUser(['name' => 'tessUserName', 'email' => 'test@email.com']);
$updateNote = $userService->updateNote($user->getId(), 'test_note');

var_dump($user);
var_dump($updateNote);
var_dump($inMemoryUserEventsRepository->getStorage());

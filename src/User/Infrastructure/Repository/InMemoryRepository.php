<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Repository;

use App\User\Model\Exceptions\InMemoryRepositoryException;
use App\User\Model\User;
use App\User\Model\UserRepositoryInterface;

class InMemoryRepository implements UserRepositoryInterface
{
    private array $storage = [];
    private int $autoIncrementCounterInMemory = 0;

    public function save(User $user): User
    {
        if (null === $user->getId())  {
            $this->autoIncrementCounterInMemory++;
            $user->setId($this->autoIncrementCounterInMemory);
            $this->storage[User::class][$this->autoIncrementCounterInMemory] = serialize($user);

            return $user;
        }

        $userInStorage = $this->findById($user->getId());
        if (null === $userInStorage) {
            throw new InMemoryRepositoryException('User not found');
        }

        $this->storage[User::class][$userInStorage->getId()] = serialize($user);

        return $user;
    }

    public function findById(int $userId): ?User
    {
        if (\array_key_exists($userId, $this->storage[User::class])) {
            return \unserialize($this->storage[User::class][$userId]);
        }

        return null;
    }

    public function findUserByEmailOrName(string $email, string $name): ?User
    {
        foreach ($this->storage[User::class] ?? [] as $user) {
            /** @var User $userModel */
            $userModel = \unserialize($user);
            if ($email === $userModel->getEmail() || $name === $userModel->getName()) {
                return $userModel;
            }
        }

        return null;
    }
}

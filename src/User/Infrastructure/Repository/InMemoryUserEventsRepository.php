<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Repository;

use App\User\Model\UserEventsRepositoryInterface;

class InMemoryUserEventsRepository implements UserEventsRepositoryInterface
{
    private array $storage = [];

    public function addEvent(string $eventType, int $userId, string $payload): void
    {
        $event['type'] = $eventType;
        $event['payload'] = $payload;

        $this->storage[$userId][] = $event;
    }

    /**
     * @return array
     */
    public function getStorage(): array
    {
        return $this->storage;
    }
}

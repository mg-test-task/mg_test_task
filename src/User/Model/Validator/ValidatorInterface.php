<?php

declare(strict_types=1);

namespace App\User\Model\Validator;

interface ValidatorInterface
{
    /**
     * @return bool
     */
    public function isValid(): bool;
}

<?php

declare(strict_types=1);

namespace App\User\Model\Validator;

class EmailsStopList
{
    private const EMAILS_STOP_LIST = [
        'example1@email.com',
        'example2@email.com',
    ];

    public static function getEmailsStopList(): array
    {
        return self::EMAILS_STOP_LIST;
    }
}

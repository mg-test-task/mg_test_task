<?php

declare(strict_types=1);

namespace App\User\Model\Validator;

class EmailValidator implements ValidatorInterface
{
    private string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function isValid(): bool
    {
        return $this->isValidEmail() && !$this->isEmailInStopList();
    }

    private function isValidEmail(): bool
    {
        if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        return false;
    }

    private function isEmailInStopList(): bool
    {
        return \in_array($this->email, EmailsStopList::getEmailsStopList(), true);
    }
}

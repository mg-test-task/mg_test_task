<?php

declare(strict_types=1);

namespace App\User\Model\Validator;

class UserNameStopList
{
    private const USER_NAME_STOP_LIST = [
        'exampleName1',
        'exampleName2',
    ];

    public static function getUsersNameStopList(): array
    {
        return self::USER_NAME_STOP_LIST;
    }
}

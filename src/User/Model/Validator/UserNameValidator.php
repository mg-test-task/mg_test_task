<?php

declare(strict_types=1);

namespace App\User\Model\Validator;

class UserNameValidator implements ValidatorInterface
{
    private const MIN_NAME_LENGTH = 8;

    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function isValid(): bool
    {
        return $this->isValidNameLength() &&
            $this->isValidName() &&
            !$this->isNameInStopList();
    }

    private function isValidNameLength(): bool
    {
        return mb_strlen($this->name) >= self::MIN_NAME_LENGTH;
    }

    private function isValidName(): bool
    {
        return 1 === preg_match('/^[a-zA-Z0-9]+$/', $this->name);
    }

    private function isNameInStopList(): bool
    {
        return \in_array($this->name, UserNameStopList::getUsersNameStopList(), true);
    }
}

<?php

declare(strict_types=1);

namespace App\User\Model;

class User
{
    private ?int $id = null;
    private string $name;
    private string $email;
    private string $created;
    private ?string $deleted;
    private ?string $notes;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @return string|null
     */
    public function getDeleted(): ?string
    {
        return $this->deleted;
    }

    public function isDeleted(): bool
    {
        return null !== $this->deleted;
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string $name
     *
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $created
     *
     * @return User
     */
    public function setCreated(string $created): User
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @param string|null $deleted
     *
     * @return User
     */
    public function setDeleted(?string $deleted): User
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @param string|null $notes
     *
     * @return User
     */
    public function setNotes(?string $notes): User
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;

        return $this;
    }
}

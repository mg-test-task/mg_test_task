<?php

declare(strict_types=1);

namespace App\User\Model\Exceptions;

class UserException extends \Exception
{
}
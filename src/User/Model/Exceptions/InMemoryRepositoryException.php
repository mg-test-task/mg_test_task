<?php

declare(strict_types=1);

namespace App\User\Model\Exceptions;

class InMemoryRepositoryException extends \Exception
{
}
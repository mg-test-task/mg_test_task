<?php

declare(strict_types=1);

namespace App\User\Model;

interface UserRepositoryInterface
{
    /**
     * @param User $model
     */
    public function save(User $model): User;

    /**
     * @param int $userId
     *
     * @return User
     */
    public function findById(int $userId): ?User;

    /**
     * @param string $email
     * @param string $name
     *
     * @return User|null
     */
    public function findUserByEmailOrName(string $email, string $name): ?User;
}

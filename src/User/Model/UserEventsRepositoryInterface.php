<?php

declare(strict_types=1);

namespace App\User\Model;

interface UserEventsRepositoryInterface
{
    public const EVENT_TYPE_USER_CREATED  = 'user_created';
    public const EVENT_TYPE_NOTES_UPDATED = 'notes_updated';
    public const EVENT_TYPE_USER_DELETED  = 'user_deleted';

    /**
     * @param string $eventType
     * @param int    $userId
     * @param string $payload
     *
     * @return mixed
     */
    public function addEvent(string $eventType, int $userId, string $payload): void;
}

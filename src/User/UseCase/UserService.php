<?php

declare(strict_types=1);

namespace App\User\UseCase;

use App\User\Model\Exceptions\UserException;
use App\User\Model\User;
use App\User\Model\UserEventsRepositoryInterface;
use App\User\Model\UserRepositoryInterface;
use App\User\Model\Validator\EmailValidator;
use App\User\Model\Validator\UserNameValidator;

class UserService
{
    private UserRepositoryInterface $userRepository;
    private UserEventsRepositoryInterface $userEventsRepository;

    public function __construct(UserRepositoryInterface $userRepository, UserEventsRepositoryInterface $userEventsRepository)
    {
        $this->userRepository = $userRepository;
        $this->userEventsRepository = $userEventsRepository;
    }

    public function createUser(array $request): User
    {
        $userValidator = new UserNameValidator($request['name']);
        $emailValidator = new EmailValidator($request['email']);

        if (!$userValidator->isValid()) {
            throw new UserException('Invalid name');
        }

        if (!$emailValidator->isValid()) {
            throw new UserException('Invalid email');
        }

        $user = $this->userRepository->findUserByEmailOrName($request['email'], $request['name']);

        if (null !== $user) {
            throw new UserException('User already exists');
        }

        $user = (new User())
            ->setName($request['name'])
            ->setEmail($request['email'])
            ->setCreated((string)\time()); //time() only for example, because we skip real db implementation

        $createdUser = $this->userRepository->save($user);
        $this->userEventsRepository->addEvent(UserEventsRepositoryInterface::EVENT_TYPE_USER_CREATED, $createdUser->getId(), serialize($createdUser));

        return $createdUser;
    }

    public function updateNote(int $userId, string $notes): User
    {
        $user = $this->userRepository->findById($userId);
        if (null === $user) {
            throw new UserException('User not found');
        }

        $user->setNotes($notes);

        $updatedUser = $this->userRepository->save($user);
        $this->userEventsRepository->addEvent(UserEventsRepositoryInterface::EVENT_TYPE_NOTES_UPDATED, $updatedUser->getId(), serialize($updatedUser));

        return $updatedUser;
    }

    public function markUserAsDeleted(int $userId): User
    {
        $user = $this->userRepository->findById($userId);
        if (null === $user) {
            throw new UserException('User not found');
        }

        $time = (string)\time();
        if ($user->getCreated() < $time) {
            throw new UserException('Deleted timestamp can\'t more than crated timestamp');
        }

        $user->setDeleted($time); //time() only for example, because we skip real db implementation

        $updatedUser = $this->userRepository->save($user);
        $this->userEventsRepository->addEvent(UserEventsRepositoryInterface::EVENT_TYPE_USER_DELETED, $updatedUser->getId(), serialize($updatedUser));

        return $updatedUser;
    }
}

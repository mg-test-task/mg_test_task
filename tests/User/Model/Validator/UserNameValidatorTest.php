<?php

declare(strict_types=1);

use App\User\Model\Validator\UserNameStopList;
use App\User\Model\Validator\UserNameValidator;
use PHPUnit\Framework\TestCase;

class UserNameValidatorTest extends TestCase
{
    public function testCorrectUserNameShouldSuccess(): void
    {
        $email = 'userName';
        $result = (new UserNameValidator($email))->isValid();

        self::assertTrue($result);
    }

    public function testShortUserNameShouldFail(): void
    {
        $email = 'name';
        $result = (new UserNameValidator($email))->isValid();

        self::assertFalse($result);
    }

    public function testIncorrectPatternUserNameShouldFail(): void
    {
        $email = 'userNameExample&&&';
        $result = (new UserNameValidator($email))->isValid();

        self::assertFalse($result);
    }

    public function testUserNameFromStopListShouldFail(): void
    {
        $email = UserNameStopList::getUsersNameStopList()[0];
        $result = (new UserNameValidator($email))->isValid();

        self::assertFalse($result);
    }
}

<?php

declare(strict_types=1);

use App\User\Model\Validator\EmailsStopList;
use App\User\Model\Validator\EmailValidator;
use PHPUnit\Framework\TestCase;

class EmailValidatorTest extends TestCase
{
    public function testCorrectEmailShouldSuccess(): void
    {
        $email = 'test@test.com';
        $result = (new EmailValidator($email))->isValid();

        self::assertTrue($result);
    }

    public function testIncorrectEmailShouldFail(): void
    {
        $email = 'email_test.com';
        $result = (new EmailValidator($email))->isValid();

        self::assertFalse($result);
    }

    public function testEmailFromStopListShouldFail(): void
    {
        $email = EmailsStopList::getEmailsStopList()[0];
        $result = (new EmailValidator($email))->isValid();

        self::assertFalse($result);
    }
}

<?php

declare(strict_types=1);

use App\User\Model\Exceptions\UserException;
use App\User\Model\User;
use App\User\Model\UserEventsRepositoryInterface;
use App\User\Model\UserRepositoryInterface;
use App\User\UseCase\UserService;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    public function testCreateUserShouldSuccess(): void
    {
        $useRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();
        $useRepositoryMock->method('findById')->willReturn(null);
        $useRepositoryMock->method('save')->willReturn(
            (new User())->setId(10)->setEmail('testemail@test.com')->setName('testName')
        );

        $request = [
            'name' => 'testName',
            'email' => 'testemail@test.com'
        ];

        $result = (new UserService($useRepositoryMock, $userEventsRepositoryMock))->createUser($request);

        self::assertEquals($request['name'], $result->getName());
        self::assertEquals($request['email'], $result->getEmail());
        self::assertNotNull($result->getId());
    }

    public function testCreateUserWithIncorrectNameShouldFail(): void
    {
        $useRepositoryMock = $this->createUserRepositoryMock();

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();

        $useRepositoryMock->method('findById')->willReturn(null);

        $request = [
            'name' => 'testName***',
            'email' => 'testemail@test.com'
        ];

        $this->expectException(UserException::class);
        $this->expectExceptionMessage('Invalid name');

        (new UserService($useRepositoryMock, $userEventsRepositoryMock))->createUser($request);
    }

    public function testCreateUserWithIncorrectEmailShouldFail(): void
    {
        $useRepositoryMock = $this->createUserRepositoryMock();
        $useRepositoryMock->method('findById')->willReturn(null);

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();

        $request = [
            'name' => 'testName',
            'email' => 'testemailtest.com'
        ];

        $this->expectException(UserException::class);
        $this->expectExceptionMessage('Invalid email');

        (new UserService($useRepositoryMock, $userEventsRepositoryMock))->createUser($request);
    }

    public function testCreateUserWhenEmailExistsEmailShouldFail(): void
    {
        $userRepositoryMock = $this->createUserRepositoryMock();

        $request = [
            'name' => 'testName',
            'email' => 'test@email.com'
        ];
        $user = new User();
        $user->setId(123)
            ->setEmail($request['email'])
            ->setName('name');

        $userRepositoryMock->method('findUserByEmailOrName')->willReturn($user);

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();

        $this->expectException(UserException::class);
        $this->expectExceptionMessage('User already exists');

        (new UserService($userRepositoryMock, $userEventsRepositoryMock))->createUser($request);
    }

    public function testUpdateNoteShouldSuccess(): void
    {
        $useRepositoryMock = $this->createUserRepositoryMock();
        
        $user = (new User())->setId(123)
            ->setEmail('test@email.com')
            ->setName('name');

        $useRepositoryMock->method('findById')->willReturn($user);
        $useRepositoryMock->method('save')->willReturn($user);

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();
        $notes = 'test_note';

        $userResult = (new UserService($useRepositoryMock, $userEventsRepositoryMock))->updateNote($user->getId(), $notes);

        self::assertEquals($notes, $userResult->getNotes());
    }

    public function testUpdateNoteShouldFailWhenUserNotFound(): void
    {
        $useRepositoryMock = $this->createUserRepositoryMock();
        $useRepositoryMock->method('findById')->willReturn(null);

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();

        $this->expectException(UserException::class);
        $this->expectExceptionMessage('User not found');

        (new UserService($useRepositoryMock, $userEventsRepositoryMock))->updateNote(123, 'test_note');
    }

    public function testMarkUserAsDeletedShouldFailWhenUserNotFound(): void
    {
        $useRepositoryMock = $this->createUserRepositoryMock();
        $useRepositoryMock->method('findById')->willReturn(null);

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();

        $this->expectException(UserException::class);
        $this->expectExceptionMessage('User not found');

        (new UserService($useRepositoryMock, $userEventsRepositoryMock))->markUserAsDeleted(123);
    }

    public function testMarkUserAsDeletedShouldSuccess(): void
    {
        $userRepositoryMock = $this->createUserRepositoryMock();

        $user = ( new User())->setId(123)
            ->setEmail('test@email.com')
            ->setName('name')
            ->setCreated((string)\time());

        $userRepositoryMock->method('findById')->willReturn($user);
        $userRepositoryMock->method('save')->willReturn($user);

        $userEventsRepositoryMock = $this->createUserEventsRepositoryMock();

        $result = (new UserService($userRepositoryMock, $userEventsRepositoryMock))->markUserAsDeleted(123);

        self::assertTrue($result->isDeleted());
    }

    private function createUserEventsRepositoryMock()
    {
        return $this->getMockBuilder(UserEventsRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function createUserRepositoryMock()
    {
        return $this->getMockBuilder(UserRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
